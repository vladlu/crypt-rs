use std::char::*;
use std::char::REPLACEMENT_CHARACTER;
use std::io::{stdin, Write}; 
use std::io;
use std::env::args;

use rand::prelude::*;
fn main() {
    let args = args().collect::<Vec<String>>();
    println!("{}",gcd(1820,231));
    if args.len() < 2 {
        eprintln!("
                No arguments provided!
                USAGE: crypt-rs [encrypt|decrypt]
                  ");
                  std::process::exit(1);
    }
   match args.clone()[1].as_str() {
        "encrypt" | "e" => run_encrypt(),
        "decrypt"| "d" => run_decrypt(),
        "key-encrypt" | "ke" => run_encrypt_with_key(),
        "fe" | "file-encrypt" => {
            if args.len() >= 3 {
            encrypt_with_file(args[2].clone())
        } else {  
            eprintln!("No file provided!");
        std::process::exit(1)}},
        "de" | "file-decrypt" => {
            if args.len() >= 3 {
            decrypt_file(args[2].clone())                
            } else {
            eprintln!("No file provided!");
            std::process::exit(1)}},
         _ => {eprintln!("Wrong argument provided, check spelling!");
                                            std::process::exit(1)},
    }
      
}

fn run_encrypt() {
    println!("Insira uma frase:");
    let inputstr = io_read().expect("Could not get input!");
    println!("Qual o tamanho da sua chave? (número)");
    let keysize = io_read().expect("Could not get input!").parse::<i32>().expect("Input is not a number!");
    let mut res = encrypt(inputstr.clone(), gen_key(keysize));
    println!("\nComputing...");
    if res.is_err() {
        while res.is_err() {
            res = encrypt(inputstr.clone(), gen_key(keysize))
        }
    }
    let (encrypted,key) = res.unwrap();
    println!(
        "
        Encrypted Data :      {}
        Key            :      {}
        ", encrypted,key);
}

fn run_decrypt() {
    println!("Insira os seus dados encriptados:");
    let inputstr = io_read().expect("Could not get input!");
    println!("Insira a sua chave:");
    let key = io_read().expect("Could not get input!");
    let decrypted = decrypt(inputstr, key);
    println!("
        Decrypted Data :      {}
        ", decrypted);

}
fn run_encrypt_with_key() {
    println!("Insira uma frase:");
    let inputstr = io_read().expect("Could not get input!");
    println!("Insira uma chave:");
    let key = io_read().expect("Could not get input!");
    let (encrypted,key) = match encrypt(inputstr, key){
        Ok(t) => (t.0,t.1),
        Err(e) => {
            eprintln!("{}",e);
            std::process::exit(1)
        },
    };
    println!(
        "
        Encrypted Data :      {}
        Key            :      {}
        ", encrypted,key);
}


fn encrypt_with_file(filepath :String) {
    println!("Insira o tamanho da chave:");
    let keysize = io_read().expect("Could not get input!").parse::<i32>().expect("Input is not a number!?");
    let inputstr = file_read(filepath).expect("Could not open file!"); 
    let mut res = encrypt(inputstr.clone(), gen_key(keysize));
    if res.is_err() {
        while res.is_err() {
        res = encrypt(inputstr.clone(), gen_key(keysize))
        }
    }
    let (encrypted,key) = res.unwrap();
    match file_write(encrypted, "encrypted.txt".to_owned()) {
        Ok(_) => println!("\n Chave para desencriptar:          {}", key ),
        Err(e) => eprintln!("{}",e)
    }
}

fn decrypt_file(filepath :String) {
    let inputstr = file_read(filepath).expect("Could not open file!");
    println!("Insira a sua chave:");
    let key = io_read().expect("Could not get input!");
    let decrypted = decrypt(inputstr, key);
    match file_write(decrypted, "decrypted.txt".to_owned()) {
        Ok(_) => println!("\n Ok!"),
        Err(e) => eprintln!("{}",e)
    }
}

fn io_read() -> io::Result<String> {
    let mut result = String::new();
    stdin().read_line(&mut result)?;
    Ok(result.trim().to_owned())
}

fn file_read(s: String) -> io::Result<String> {
    let contents = std::fs::read_to_string(s)?;
    Ok(contents)
}

fn file_write(s : String,status : String) -> io::Result<()> {
    let mut file = std::fs::File::create(status)?;
    file.write_all(s.as_bytes())?;
    Ok(())
}

fn get_key_char() -> Result<char,String> {
    let rng: u32 = (random::<f64>() * 256.0 ) as u32;
    let one = from_u32(rng).unwrap_or('a');
    let two = from_u32(rng + 9728).unwrap_or('a');
    let cha;
    if f64::floor(random::<f64>() + 0.5) as u32 == 0 {
        cha = one;
    }else {
        cha = two;
      //cha = one;
    }
    if cha.is_alphanumeric() || cha.is_ascii_punctuation() {
        Ok(cha)
    } else {
        Err("Not a valid char!".to_owned())
    }
}

fn gen_key(keysize: i32) -> String {
    let mut result = String::new();
    let mut k;
    for _i in 0..keysize {
        k = get_key_char();
        while k.is_err() {
            k = get_key_char()
        }
        result = format!("{}{}",result, k.unwrap_or('a'))
    }
    result
}

fn encrypt(s: String, key: String) -> Result<(String,String),String> {
    let mut finalstr = String::new();
    let keysize = (key.len() % 255) as u32;
    if s.len() > key.len(){
    for (i,ch) in s.chars().enumerate() {
        let mut curr_char = ch;
        for (j,ch1) in key.chars().enumerate() {
            if ((i % (j+1)) != 0) || i == 0 { 
            curr_char = from_u32(curr_char as u32 ^ ch1 as u32 ^ keysize).unwrap_or(REPLACEMENT_CHARACTER)
            }
        }
            finalstr.push(curr_char)
}
    } else {
        for (i,ch) in s.chars().enumerate() {
        let mut curr_char = ch;
        for (j,ch1) in key.chars().enumerate() {
            if ((j % (i+1)) != 0) || i == 0 { 
            curr_char = from_u32(curr_char as u32 ^ ch1 as u32 ^ keysize ).unwrap_or(REPLACEMENT_CHARACTER)
            }
        }
            finalstr.push(curr_char) 
}
}

for chr in finalstr.chars() {
    if chr.is_whitespace() || chr.is_control() {
        return Err("Invalid!".to_owned())
    }
} 

if decrypt(finalstr.clone(),key.clone()).trim() == s.trim() {
    Ok((finalstr,key)) 
} else {
    Err("Does not match".to_owned())
}
}
fn decrypt(s: String, key: String) -> String {
    let mut finalstr =String::new();
    let keysize = (key.len() % 255) as u32;
    if s.len() > key.len() {
    for (i,ch) in s.chars().enumerate()  {
        let mut curr_char = ch;
        for (j,ch1) in key.chars().enumerate() {
            if (i % (j+1)) != 0 || i == 0 {
            curr_char = from_u32(curr_char as u32 ^ ch1 as u32 ^ keysize).unwrap_or(REPLACEMENT_CHARACTER)
        }
    }
        finalstr.push(curr_char)
    }
} else {
for (i,ch) in s.chars().enumerate()  {
        let mut curr_char = ch;
        for (j,ch1) in key.chars().enumerate() {
            if (j % (i+1)) != 0 || i == 0 {
            curr_char = from_u32(curr_char as u32 ^ ch1 as u32 ^ keysize).unwrap_or(REPLACEMENT_CHARACTER)
        }
    }
        finalstr.push(curr_char)
    }
} 
    finalstr
}



fn gcd(a: i64,b: i64) -> i64 {
    if b == 0 {
        return a;
    }
    let n = a - b * (a /b);
    gcd(b,n)
}




